Copyright (C) 2020, Samir Faycal Tahar M'Sallem


Programmierung interaktiver Systeme HÜ01 - WS20/21

[Javadoc](https://git.thm.de/sftm31/pis_hue01/-/tree/master/doc/pis/hue1)


[Runnable](https://git.thm.de/sftm31/pis_hue01/-/raw/master/Runnable.jar)

Der Code wurde abgegeben und ist bekannt, Kopieren des Ganzen oder von Teilen kann/wird zu Plagiat führen!