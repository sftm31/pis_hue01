package pis.hue1;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI {

	// General
	private static JFrame frame = new JFrame();
	private static GridBagConstraints con = new GridBagConstraints();
	private static ButtonListener listener = new ButtonListener();
	private static InputListener ilistener = new InputListener();
	
	// Labels, Texts
	static JLabel error = new JLabel("<html><span style='font-size:10px'>Bitte Eingabe überprüfen!</span></html>");
	static JLabel header = new JLabel("<html><span style='font-size:20px'>Bitte wähle einen Algorithmus!</span></html>");
	static JLabel intro = new JLabel("<html><span style='font-size:11px'>Gebe einen Klartext oder Geheimtext ein!</span></html>");
	static JLabel success_encode = new JLabel("<html><span style='font-size:11px'>Erfolgreich verschlüsselt!</span></html>");
	static JLabel success_decode = new JLabel("<html><span style='font-size:11px'>Erfolgreich entschlüsselt!!</span></html>");
	
	// I/O Section
	static JTextArea input = new JTextArea(8, 30);
	static JTextArea output = new JTextArea(8, 30);
	static JScrollPane sp = new JScrollPane(input);
	
	// Keys
	static final JTextField key1 = new JTextField(10);
	static final JTextField key2 = new JTextField(10);
	
	// Buttons
	static JButton kodieren = new JButton("Kodieren");
	static JButton dekodieren = new JButton("Dekodieren");
	
	// Radio Buttons
	static JRadioButton choice1 = new JRadioButton ("Caesar");
	static JRadioButton choice2 = new JRadioButton ("Würfel");
	static JRadioButton choice3 = new JRadioButton ("Doppel-Würfel");
	static ButtonGroup bgroup = new ButtonGroup();

	
	public void init() {
		preferences();
		
		frame.setVisible(true);
	}
	
	
	private void preferences() {
		// Frame 
		frame.setTitle("Caesar, Würfel, Doppelwürfel - Verschlüsslung");
		frame.setSize(800, 400);
		frame.setLayout(new GridBagLayout());
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Positionierung der Components
		posComponent(5, 0, 0, 5, header);
		
			// Box für Algorithmen 
		Box algr = Box.createHorizontalBox();
		algr.add(choice1);
		algr.add(choice2);
		algr.add(choice3);
		posComponent(0, 10, 0, 10, algr);
			
		posComponent(0, 20, 0, 10, intro);
		
			// Box für Geheimschluessel
		Box keys = Box.createHorizontalBox();
		keys.add(key1);
		keys.add(key2);
		posComponent(0, 30, 0, 10, keys);
		
			// Box für Input und Output
		Box io = Box.createHorizontalBox();
		io.add(input);
		io.add(sp);
		io.add(output);
		posComponent(0, 40, 0, 20, io);
		
			// Box für Kodiere/Dekodiere Buttons
		Box buttons = Box.createHorizontalBox();
		buttons.add(kodieren);
		buttons.add(dekodieren);
		posComponent(0, 60, 0, 10, buttons);
		
		posComponent(0, 70, 0, 5, error);
		posComponent(0, 70, 0, 5, success_decode);
		posComponent(0, 70, 0, 5, success_encode);

		
		// Listener registrieren
		kodieren.addActionListener(listener);
		dekodieren.addActionListener(listener);
		key1.addFocusListener(ilistener);
		key2.addFocusListener(ilistener);
		input.addFocusListener(ilistener);
		output.addFocusListener(ilistener);
		
		//Default Texte hinzufügen
		key1.setText("Losungswort 1");
		key2.setText("Losungswort 2");
		input.setText("Klartext");
		output.setText("Geheimtext");
		
		//Radio Buttons konfigurieren
		choice1.setActionCommand("Caesar");
		choice2.setActionCommand("Würfel");
		choice3.setActionCommand("Doppel-Würfel");
		
		// Zur Buttongroup hinzufügen
		bgroup.add(choice1);
		bgroup.add(choice2);
		bgroup.add(choice3);
		
		// Action Messages default not visible
		error.setVisible(false);
		success_decode.setVisible(false);
		success_encode.setVisible(false);
		
		// Styling
		frame.getContentPane().setBackground(Color.white);		
		choice1.setBackground(Color.white);
		choice2.setBackground(Color.white);
		choice3.setBackground(Color.white);
		kodieren.setBackground(Color.cyan);
		kodieren.setBorderPainted(false);	
		dekodieren.setBackground(Color.orange);
		dekodieren.setBorderPainted(false);		
		input.setBorder(BorderFactory.createLineBorder(Color.black));
		input.setLineWrap(true);
		output.setBorder(BorderFactory.createLineBorder(Color.black));
		output.setLineWrap(true);
		error.setForeground(Color.red);
		success_encode.setForeground(Color.green);
		success_decode.setForeground(Color.green);
	}
	
	private void posComponent(int x, int y, int width, int height, JComponent c) { //Positionierung und hinzufügen der Components
		con.gridx=x;
		con.gridy=y;
		con.gridwidth=width;
		con.gridheight=height;
		frame.add(c, con);
	}
	


}

