package pis.hue1;

interface Codec {
	/**
	 * Gibt den kodierten Text zu einem gegebenen Klartext an
	 * 
	 * @param klartext String, ein Klartext, welcher durch die Methode kodiert wird
	 *		  String darf jedes mögliche Zeichen enthalten, Methode entscheidet dann, welches Zeichen übernommen wird und welches nicht.
	 *		  Zur Verarbeitung braucht die Methode einen Schluessel.
	 * @return String, modifizierten Klartext, welcher nach Vorgaben der Methode, kodiert wurde.
	 */
	public String kodiere(String klartext); 
	
	/**
	 * Gibt den dekodierten Text zu einem verschluesselten Text an.
	 * 
	 * @param geheimtext String, ein Geheimtext, welcher durch die Methode dekodiert wird.
	 *		  String, muss syntaktisch und semantisch so geformt sein, dass Methode daraus einen Klartext formen kann.
	 * @return String, Klartext
	 */
	public String dekodiere(String geheimtext); 
	
	/**
	 * Getter-Methode, Losung muss vorher gesetzt worden sein, z.b über Setter-Methode
	 * @return String, Losungs-/Schluesselwort
	 */
	public String gibLosung(); 
	
	/**
	 * Setter-Methode, Losung/Schluessel muss geeignet sein, um gesetzt werden zu können, ansonsten wird eine IllegalArgumentException geworfen.
	 * Die zu implementierenden Klassen müssen mit diesem Schlüssel umgehen können, d.h aus einem Klartext und mindestens einem Schlüssel folgt ein valider Geheimtext, bzw. aus einem Geheimtext wieder ein Klartext.
	 * @param schluessel String
	 */
	public void setzeLosung(String schluessel) throws IllegalArgumentException; 
	
}
