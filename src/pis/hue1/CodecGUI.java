package pis.hue1;

import java.awt.EventQueue;

public class CodecGUI {
	
	static Codec c1, c2;
	
	public static void main(String[] args) {
		
		c1 = new Caesar();
		c2 = new Wuerfel();
		
		Runnable r = new Runnable() {
	           @Override
	           public void run() {
	               new GUI().init();
	           }
	       };
	       
	    EventQueue.invokeLater(r);
	   
	}
}
