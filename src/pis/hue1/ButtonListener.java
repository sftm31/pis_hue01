package pis.hue1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener{

	

	@Override
	public void actionPerformed(ActionEvent e) {
		GUI.input.setBackground(Color.white);
		GUI.output.setBackground(Color.white);
		GUI.error.setVisible(false);
		GUI.success_decode.setVisible(false);
		GUI.success_encode.setVisible(false);
		
		if(e.getSource().equals(GUI.kodieren)) { //Wenn kodiere-Button gedrückt
			
			if(GUI.kodieren.getText() == "" || GUI.key1.getText().equals("") || GUI.key1.getText().equals("Losungswort 1")) {
				GUI.error.setVisible(true);
			} else {
				doEncode(GUI.input.getText(), GUI.key1.getText(), GUI.key2.getText(), GUI.bgroup.getSelection().getActionCommand());
			}		
		}
		
		if(e.getSource().equals(GUI.dekodieren)) { //Wenn dekodiere-Button gedrückt
			
			if(GUI.dekodieren.getText() == "" || GUI.key1.getText().equals("") || GUI.key1.getText().equals("Losungswort 1")) {
				GUI.error.setVisible(true);
			} else {
				doDecode(GUI.output.getText(), GUI.key1.getText(), GUI.key2.getText(), GUI.bgroup.getSelection().getActionCommand());
			}
		}					
	}
	
	private void doEncode(String input, String key1, String key2, String method) { // Methode zur Kodierung 
		 switch(method){ // Führe Schritte für jeweiliges Verfahren aus, setze kodierten Text
	        case "Caesar":
	        	CodecGUI.c1.setzeLosung(key1);
	            GUI.output.setText(CodecGUI.c1.kodiere(input));
	            outputAction();
	            break;
	        case "Würfel": 
	        	CodecGUI.c2.setzeLosung(key1);
	            GUI.output.setText(CodecGUI.c2.kodiere(input));
	            outputAction();
	            break;
	        case "Doppel-Würfel": 
	        	if(GUI.key2.getText().equals("") || GUI.key2.getText().equals("Losungswort 2")) {
	        		GUI.error.setVisible(true);
	        	} else {
	        		CodecGUI.c2.setzeLosung(key1);
		        	String temp = CodecGUI.c2.kodiere(input);
		        	CodecGUI.c2.setzeLosung(key2);
		            GUI.output.setText(CodecGUI.c2.kodiere(temp));
		            outputAction();
	        	}
	            break;
		 }
		
		
	}
	
	private void outputAction() {
		GUI.output.setBackground(Color.green);
		GUI.success_encode.setVisible(true);
	}

	private void doDecode(String input, String key1, String key2, String method) { // Methode zur Dekodierung 
		 switch(method){ // Führe Schritte für jeweiliges Verfahren aus, setze dekodierten Text
		 	case "Caesar":
		 		CodecGUI.c1.setzeLosung(key1);
	            GUI.input.setText(CodecGUI.c1.dekodiere(input));
	            inputAction();
	            break;
		 	case "Würfel": 
		 		CodecGUI.c2.setzeLosung(key1);
	            GUI.input.setText(CodecGUI.c2.dekodiere(input));
	            inputAction();
	            break;
		 	case "Doppel-Würfel": 
		 		if(GUI.key2.getText().equals("") || GUI.key2.getText().equals("Losungswort 2")) {
	        		GUI.error.setVisible(true);
	        	} else {
	        		CodecGUI.c2.setzeLosung(key2);
		        	String temp = CodecGUI.c2.dekodiere(input);
		        	CodecGUI.c2.setzeLosung(key1);
		            GUI.input.setText(CodecGUI.c2.dekodiere(temp));
		            inputAction();
	        	}
	            break;
		 }
	}
	
	private void inputAction() {
		GUI.input.setBackground(Color.green);
		GUI.success_decode.setVisible(true);
	}
	
}
