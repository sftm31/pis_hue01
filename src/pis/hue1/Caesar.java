package pis.hue1;

public class Caesar implements Codec{
	
	/**
	 * String, welcher Schluessel/Losungswort bildet.
	 * Wird durch den Übergabeparameter der dazugehörigen Setter-Methode initialisiert. 
	 * Prämissen die dieser erfüllen muss, wurden in {@link pis.hue1.Caesar#setzeLosung(String)} erläutert.
	 */
	private String schluessel;
	
	/**
	 * Integer-Wert, der zu jedem Integer Wert eines Buchstabens addiert oder suntrahiert wird.
	 * Wird durch die Länge des Schluesselwortes generiert. 
	 * Näheres wurde in {@link pis.hue1.Caesar#setzeLosung(String)} vordefiniert.
	 */
	private int shift; 
	
	/**
	 * String, welcher den modifizierten Text bildet, (umg. entschluesselter Text bzw. verschluesselter Text), welcher nach dem Aufrufen einer der untenstehenden Methoden zurückgegeben wird. 
	 * @see pis.hue1.Caesar#kodiere(String)
	 * @see pis.hue1.Caesar#dekodiere(String)
	 */
	private StringBuilder mod_text; 
	
	
	public Caesar() {}
	
	public Caesar(String schluessel) { 
		setzeLosung(schluessel);
	}

	/**
	 * Kodiert den Klartext, indem jeder Buchstabe innerhalb des Strings geshiftet wird (Buchstabe wird im Alphabet verschoben). Dieser Shift ist einheitlich und wird durch die Variable shift generiert.
	 * Dieser Shift wird zum Integer-Wert jedes Characters addiert und dann wird dieser neue Wert zu einem Character gecastet.
	 * Dabei werden <b>lediglich</b> die Zeichen kodiert, welche alphabetisch sind. 
	 * <b>Aus einem Buchstaben kann immer nur ein neuer Buchstabe folgen</b>, egal groß die Variable shift ist.
	 * Der Shift muss vorher definiert worden sein, indem ein valides Schluesselwort gesetzt wurde.
	 * @see pis.hue1.Caesar#setzeLosung(String)
	 * @see java.lang.Character#isAlphabetic(int)
	 * @param klartext ein String in jeder Form
	 * @return String, ein Geheimtext, welcher sich aus den kodierten Zeichen zusammensetzt, die an der gleichen Stelle wie zuvor stehen und nur als Einzelnes kodiert wurden.
	 * 
	 */
	@Override
	public String kodiere(String klartext) {
		mod_text = new StringBuilder(klartext.length()); 
		char shifted_c;
		
		for(char c : klartext.toCharArray()) { 
			
			if(Character.isAlphabetic(c)) { //ABCXYZ123 //THM -> 3 //DEFABC123 
				shifted_c = (char) (c+shift);
				
				if(Character.isAlphabetic(shifted_c) && Character.getType(c) == Character.getType(shifted_c)) {
					mod_text.append(shifted_c);
				} else {
					mod_text.append((char) (shifted_c - 26));
				}
				
			} else {
				mod_text.append(c);
			}
			
		}
		return mod_text.toString(); 
	}

	/**
	 * Dekodiert den Geheimtext, indem jeder Buchstabe innerhalb des Strings um den Shift verringert wird. Der Shift ist einheitlich und wird durch die Variable shift generiert.
	 * Dieser Shift wird vom Integer-Wert jedes Characters abgezogen und dann wird dieser neue Wert zu einem Character gecastet.
	 * Dabei werden <b>lediglich</b> die Zeichen dekodiert, welche alphabetisch sind. 
	 * <b>Aus einem Buchstaben kann immer nur ein neuer Buchstabe folgen</b>, egal groß die Variable shift ist.
	 * Der Shift muss vorher definiert worden sein, indem ein valides Schluesselwort gesetzt wurde.
	 * @see pis.hue1.Caesar#setzeLosung(String)
	 * @see java.lang.Character#isAlphabetic(int)
	 * @param geheimtext ein String, ohne syntaktische Einschränkungen, der aber dekodiert semantisch richtig ist.
	 * @return String, ein Klartext, welcher sich aus den dekodierten Zeichen zusammensetzt, die an der gleichen Stelle wie zuvor stehen und nur als Einzelnes dekodiert wurden.
	 */
	@Override
	public String dekodiere(String geheimtext) {
		
		mod_text = new StringBuilder(geheimtext.length()); 
		char shifted_c;
		
		for(char c : geheimtext.toCharArray()) { 
			
			if(Character.isAlphabetic(c)) {
				shifted_c = (char) (c-shift);
				
				if(Character.isAlphabetic(shifted_c) && Character.getType(c) == Character.getType(shifted_c)) {
					mod_text.append(shifted_c);
				} else {
					mod_text.append((char) (shifted_c + 26));
				}
				
			} else {
				mod_text.append(c);
			}
		}
		
		return mod_text.toString(); 

	}
	
	/**
	 * <b>Getter-Methode</b> liefert Losung/Schluessel, welcher vorher definiert wurde.
	 * Sollte diese nicht definiert worden sein, so wird ein standardmäßiger Schlüssel "ABC" gesetzt
	 * @return String schluessel
	 * @see pis.hue1.Caesar#setzeLosung(String schluessel)
	 */
	@Override
	public String gibLosung() { 
		if(schluessel == null) { 
			setzeLosung("ABC"); 
		}
		return schluessel; 
	}

	/**
	 * <b>Setter-Methode</b> setzt Schluessel aus Übergabeparameter
	 * Da bei Caesar Verschlüsselung nur die Länge also der "Shift" wichtig ist, ist die einzige Vorraussetzung, dass der String <b> die Länge 1 oder mehr hat</b>.
	 * Nachfolgend wird diese Länge ermittelt und in der Variable shift gespeichert. Diese wird anschließend im Prozess der Kodierung und der Dekodierung benötigt.
	 * @param schluessel String
	 * @see pis.hue1.Caesar#kodiere(String)
	 * @see pis.hue1.Caesar#dekodiere(String)
	 */
	@Override
	public void setzeLosung(String schluessel) throws IllegalArgumentException {
		
		if(schluessel.length() > 0) { 
			this.schluessel=schluessel;
			shift = schluessel.length(); 
		} else {
			throw new IllegalArgumentException("Bitte verwende einen validen Schlüssel!");
		}
		
		
		
	}

}
