package pis.hue1;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class InputListener implements FocusListener{

	@Override
	public void focusGained(FocusEvent e) {
		if(e.getSource().equals(GUI.key1) && GUI.key1.getText().equals("Losungswort 1")) {
			GUI.key1.setText("");
		}
		if(e.getSource().equals(GUI.key2) && GUI.key2.getText().equals("Losungswort 2")) {
			GUI.key2.setText("");
		}
		
		if(e.getSource().equals(GUI.output) || e.getSource().equals(GUI.input)) {
			GUI.input.setBackground(Color.white);
			GUI.output.setBackground(Color.white);
		}
		
		if(e.getSource().equals(GUI.input) && GUI.input.getText().equals("Klartext")) {
			GUI.input.setText("");
		}
		
		if(e.getSource().equals(GUI.output) && GUI.output.getText().equals("Geheimtext")) {
			GUI.output.setText("");
		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

}
