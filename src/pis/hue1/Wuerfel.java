package pis.hue1;

public class Wuerfel implements Codec{

	/**
	 * String, welcher Schluessel/Losungswort bildet.
	 * Wird durch den Übergabeparameter der dazugehörigen Setter-Methode initialisiert. 
	 * Prämissen die dieser erfüllen muss, wurden in {@link pis.hue1.Wuerfel#setzeLosung(String)} erläutert.
	 */
	private String schluessel; 
	
	/**
	 * Integer Array einer Permutation, welche mit Hilfe der Methode {@link pis.hue1.Wuerfel#setzeLosung(String)} aus dem Losungswort generiert wurde. Diese spielt eine große Rolle bei der Verschluesslung und Entschluesslung.
	 * Diese enthält jeweils die Zahlen von 1 bis zur Länge des Schluesselwortes. Sie nummeriert die Zeichen in ihrer alphabetischen Reihenfolge und bildet so eine Permutation.
	 * @see pis.hue1.Wuerfel#kodiere(String)
	 * @see pis.hue1.Wuerfel#dekodiere(String)
	 */
	private int[] permutation; 
	
	/**
	 * String, welcher den modifizierten Text bildet, (umg. entschluesselter Text bzw. verschluesselter Text), welcher nach dem Aufrufen einer der untenstehenden Methoden zurückgegeben wird. 
	 * @see pis.hue1.Wuerfel#kodiere(String)
	 * @see pis.hue1.Wuerfel#dekodiere(String)
	 */
	private StringBuilder mod_text; 
	
	
	
	
	public Wuerfel() {}
	
	public Wuerfel(String schluessel) { //konstruktor gefordert wegen CodecTest.java
		setzeLosung(schluessel); //setze schlüssel des Wuerfel-Objekts 
	}
	
	/**
	 * Kodiere den Klartext mit dem Wuerfel Verfahren. Nehme dazu die Permutation, welche durch den vorher definierten Schluessel generiert wurde und verschiebe alle Zeichen innerhalb des des Wortes so.
	 * Dabei werden lediglich die Positionen der Zeichen vertauscht, die Zeichen selbst werden aber nicht modifiziert.
	 * Daraus folgt, dass alle Zeichen verschoben werden, der Typ spielt dabei keine Rolle.
	 * @param klartext String, ein Klartext in jeder Form.
	 * @return String, kodierter Geheimtext mit dem Wuerfel-Verfahren verschluesselt.
	 */
	@Override
	public String kodiere(String klartext) {
		
		mod_text = new StringBuilder(klartext.length()); // nun ist es ein StringBuilder und kein char-Array mehr!
		int start=1, i=0; //lege zähler variablen fest
		
		while(i<permutation.length) {
			if(permutation[i] == start) { //wenn stelle eintrag bei reihenfolge[i] mit start übereinstimmt so wurde der erste eintrag gefunden
				
				for(int x=i; x<klartext.length(); x+=permutation.length) { 
					mod_text.append(klartext.charAt(x)); //füge zum geheimtext alle zeichen hinzu an stelle x, x+länge, x+2*länge usw.
				}
				start++; //erhöhe start auf 2, nun suche nach der zahl 2
				i=0; //springe zurück an den anfang des arrays um dort mit der suche zu beginnen
			} else {
				i++; //erhöhe wenn die zahl nicht die gesuchte ist
			}
			
		}
		return mod_text.toString();
	}

	/**
	 * Dekodiere den Klartext mit dem Wuerfel Verfahren. Nehme dazu die Permutation, welche durch den vorher definierten Schluessel generiert wurde und verschiebe alle Zeichen wieder an ihre eigentliche Stelle zurück.
	 * Lediglich die Positionen dieser Zeichen werden dabei verändert.
	 * @param geheimtext String, ein Geheimtext, welcher mit dem Schluessel zu einem syntaktisch korrekten Text entschluesselt werden kann. <b>Also ein Text, welcher vorher nach diesem Verfahren kodiert wurde.</b>
	 * @return String, dekodierter Geheimtext mit dem Wuerfel-Verfahren entschluesselt.
	 */
	@Override
	public String dekodiere(String geheimtext) {
		
		mod_text = new StringBuilder(geheimtext); // setze String auf geheimtext
		
		int start=1, i=0; //lege zähler variablen fest
		int x=0;
		
		while(i<permutation.length) { //solange bis es am ende des wortes ankommt
			if(permutation[i] == start) { //wenn die stelle i dem startparameter entspricht so beginne mit korrektur des geheimtext
				
				for(int y=i; y<geheimtext.length(); y+=permutation.length) { //bsp: die zahl 1 wird an stelle 2 gefunden, also folgt, dass die zeichen an stelle 0 bis x eigentlich an stelle 2, 2+schlüsselwortlänge, usw. müssten.
					mod_text.deleteCharAt(y); //lösche bisherigen eintrag in string
					mod_text.insert(y, geheimtext.charAt(x)); //setze "richtigen" eintrag an der stelle
					x++;
				}
				start++; //wenn alle zeichen in der spalte 1 abgearbeitet wurden, setze auf 2
				i=0; //setze zeiger zurück auf ersten eintrag im reihenfolgen array, damit von vorne gesucht wird
			} else {
				i++; //wenn die zahl nicht übereinstimmt so springe an die nächste stelle von reihenfolge[]
			}
		}

		return mod_text.toString();
	}

	/**
	 * Getter-Methode, Ist die Losung/der Schluessel vorher nicht gesetzt wurden, so setze Schluessel "default".
	 * @return String, Losungs-/Schluesselwort
	 */
	@Override
	public String gibLosung() { //getter methode
		if(schluessel == null) { //sollte vorher kein schlüssel festgelegt worden sein, so nutze Schlüssel "Default"
			setzeLosung("default"); 
		}
		return schluessel; //gebe schlüssel zurück
	}

	/**
	 * Darf nur Groß- und Kleinbuchstaben enthalten. Wortlänge muss größer als 1 sein, da sonst keine Permutation generiert werden kann.
	 * Werfe eine Exception wenn dies nicht erfüllt ist.
	 * @param schluessel Muss obige Prämissen erfüllen
	 */
	@Override
	public void setzeLosung(String schluessel) throws IllegalArgumentException {
		if(schluessel.length() > 1 && schluessel.matches("[a-zA-Z]+")) {  //darf nur buchstaben von a-z bzw. A-Z enthalten und muss größer 1 sein, da sonst keine sortierung erfolgen kann!
			
			this.schluessel = schluessel; //setze parameter als schlüssel
			
			String temp = schluessel.toLowerCase(); //um sortierung vorzunehmen
			permutation = new int[temp.length()];
			int i=97, zaehler=1;
			
			while(i<123) { //gehe alphabet durch
				for(int y=0; y<temp.length(); y++) { //gehe jede stelle des Schlüssels durch
					if(temp.charAt(y) == i) { //wenn zeichen an der stelle dem zeichen im alphabet entspricht, dann setze dort den zähler
						permutation[y] = zaehler;
						zaehler++; //erhöhe zähler um 1, da dieser ja gesetzt wurde
					}
				}
				i++; //erhöhe zeichen im alphabet wenn zeichen nicht mehr gefunden wurde
			}
		} 
		else { //wenn prämisse nicht erfüllt, dann gebe exception aus
			throw new IllegalArgumentException("Bitte verwende einen validen Schlüssel!");
		}
	}

}
